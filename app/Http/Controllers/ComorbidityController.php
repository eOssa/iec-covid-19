<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\ComorbiditiesRepositoryInterface;
use App\Http\Resources\ComorbidityResource;
use App\Models\Comorbidity;
use Illuminate\Http\Request;

class ComorbidityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ComorbiditiesRepositoryInterface $comorbiditiesRepository)
    {
        return ComorbidityResource::collection($comorbiditiesRepository->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comorbidity = Comorbidity::create(['name' => $request->input('name')]);
        return new ComorbidityResource($comorbidity);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Comorbidity  $comorbidity
     * @return \Illuminate\Http\Response
     */
    public function show(Comorbidity $comorbidity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Comorbidity  $comorbidity
     * @return \Illuminate\Http\Response
     */
    public function edit(Comorbidity $comorbidity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Comorbidity  $comorbidity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comorbidity $comorbidity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comorbidity  $comorbidity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comorbidity $comorbidity)
    {
        //
    }
}
