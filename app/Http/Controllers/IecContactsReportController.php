<?php

namespace App\Http\Controllers;

use App\Models\Symptom;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Validation\Rule;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\{Alignment, Border, Fill};
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class IecContactsReportController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->validate([
            'contact.name' => ['required', 'string'],
            'contacts' => ['nullable', 'array'],
            'contacts.*.address' => ['required', 'string'],
            'contacts.*.age' => ['required', 'integer'],
            'contacts.*.age_measure' => ['required', 'string', Rule::in(['year', 'month', 'day'])],
            'contacts.*.contact_date' => ['nullable', 'date'],
            'contacts.*.eps' => ['required', 'string'],
            'contacts.*.first_name' => ['required', 'string'],
            'contacts.*.gender' => ['required', 'string'],
            'contacts.*.identification_type' => ['required', 'string'],
            'contacts.*.identification' => ['required', 'string'],
            'contacts.*.last_name' => ['required', 'string'],
            'contacts.*.middle_name' => ['nullable', 'string'],
            'contacts.*.primary_phone' => ['required', 'string'],
            'contacts.*.relationship_type' => ['required', 'string', Rule::in(['Familiar', 'Social', 'Laboral'])],
            'contacts.*.secondary_phone' => ['nullable', 'string'],
            'contacts.*.sample_date' => ['nullable', 'date'],
            'contacts.*.sample_result' => ['nullable'],
            'contacts.*.second_last_name' => ['nullable', 'string'],
            'iec.date' => ['required', 'date'],
            'user.city' => ['required', 'string'],
        ]);
        $defaultDateFormat = 'd/m/y';
        $userCity = $request->input('user.city');
        $iecCarbonDate = Carbon::parse($request->input('iec.date'));
        $iecTime = $iecCarbonDate->format('H:m');
        $iecDate = $iecCarbonDate->format($defaultDateFormat);
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial')->setSize(11);
        $sheet = $spreadsheet->getActiveSheet();
        $defaultPadding = 0.914;
        $defaultColumnWidth = 10 + $defaultPadding;
        $defaultRowHeight = 14;
        $sheet->getDefaultColumnDimension()->setWidth($defaultColumnWidth);
        $sheet->getDefaultRowDimension()->setRowHeight($defaultRowHeight);
        $sheet->getRowDimension('1')->setRowHeight(25);
        $sheet->getRowDimension('2')->setRowHeight(15);
        $sheet->getRowDimension('3')->setRowHeight(15);
        $sheet->getRowDimension('4')->setRowHeight(15);
        $sheet->getRowDimension('5')->setRowHeight(15);
        $sheet->getRowDimension('8')->setRowHeight($defaultRowHeight);
        $sheet->getColumnDimension('H')->setWidth(14 + $defaultPadding);
        $sheet->getColumnDimension('E')->setWidth(14.33 + $defaultPadding);
        $sheet->getColumnDimension('L')->setWidth(14.67 + $defaultPadding);
        $sheet->getColumnDimension('M')->setWidth(16.50 + $defaultPadding);
        $sheet->getSheetView()->setZoomScale(130);

        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $drawing->setPath(base_path('resources/images/Header.png'))
            ->setResizeProportional(false)
            ->setHeight(118.8)
            ->setWidth(241.2)
            ->setCoordinates('A1')
            ->setWorksheet($sheet);

        $sheet->setCellValue('D1', 'Seguimiento a contactos de casos de IRA asociados al nuevo coronavirus 2019 (COVID-19)')
            ->mergeCells('D1:O1')
            ->getStyle('D1:O1')
            ->applyFromArray([
                'alignment' => ['horizontal' => Alignment::HORIZONTAL_CENTER],
                'font' => ['bold' => true, 'size' => 20],
            ]);
        $sheet->fromArray([['FECHA EXPOSICIÓN', 'PASAJEROS '], [null, 'PERSONAL DE SALUD']], null, 'P2');
        $sheet->setCellValue('EB1', 'FECHA CONTACTO');
        $sheet->setCellValue('A7', 'IDENTIFICACION')
            ->mergeCells('A7:M7')
            ->getStyle('A7:M8')
            ->applyFromArray([
                'alignment' => ['horizontal' => Alignment::HORIZONTAL_CENTER, 'wrapText' => true],
                'borders' => [
                    'allBorders' => ['borderStyle' => Border::BORDER_THIN],
                ],
                'fill' => ['fillType' => Fill::FILL_SOLID, 'startColor' => ['rgb' => 'C5E0B3']],
                'font' => ['bold' => true],
            ]);

        for ($i = 1, $column = 18; $i <= 14; $i++, $column = $column + 8) {
            $sheet->setCellValueByColumnAndRow($column, 7, "SEGUIMIENTO DIA $i")
                ->mergeCellsByColumnAndRow($column, 7, $column + 7, 7)
                ->getStyleByColumnAndRow($column, 7, $column + 7, 8)
                ->applyFromArray([
                    'alignment' => ['horizontal' => Alignment::HORIZONTAL_CENTER, 'wrapText' => true],
                    'borders' => [
                        'allBorders' => ['borderStyle' => Border::BORDER_THIN],
                    ],
                    'fill' => [
                        'fillType' => Fill::FILL_SOLID,
                        'startColor' => ['rgb' => $i % 2 === 0 ? 'D9E2F3' : 'FBE4D5'],
                    ],
                    'font' => ['bold' => true],
                ]);
        }
        $sheet->getStyle('DZ7:EB8')->applyFromArray([
            'alignment' => ['horizontal' => Alignment::HORIZONTAL_CENTER, 'wrapText' => true],
            'borders' => [
                'allBorders' => ['borderStyle' => Border::BORDER_THIN],
            ],
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'startColor' => ['rgb' => 'D9E2F3'],
            ],
            'font' => ['bold' => true],
        ]);
        $sheet->getStyle('N7:Q8')->applyFromArray([
            'alignment' => ['horizontal' => Alignment::HORIZONTAL_CENTER, 'wrapText' => true],
            'borders' => [
                'allBorders' => ['borderStyle' => Border::BORDER_THIN],
            ],
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'startColor' => ['rgb' => 'BDD6EE'],
            ],
            'font' => ['bold' => true],
        ]);
        $sheet->fromArray([
            [
                'RESPONSABLE DEL SEGUIMIENTO',
                'TIPO DE CONTACTO',
                'PRIMER N0MBRE',
                'SEGUNDO NOMBRE',
                'PRIMER APELLIDO',
                'SEGUNDO APELLIDO',
                'TIPO DE DOCUMENTO',
                '# DOCUMENTO',
                'EDAD',
                'UNIDAD DE MEDIDA',
                'SEXO (F/M)',
                'TELEFONOS',
                'ASEGURADORA',
                'ANTECEDENTE DE VIAJE',
                'PAÍSES/CIUDADES VISITADOS ',
                'TOMA DE MUESTRA',
                'RESULTADO',
            ]
        ], null, 'A8');
        $monitoringColumns = [];
        $columns = [
            'FECHA',
            'HORA',
            'ASINTOMÁTICO',
            'FIEBRE CUANTIFICADA ≥ 38°C',
            'TOS',
            'DIFICULTAD RESPIRATORIA',
            'ODINOFAGIA',
            'FATIGA/ ADINAMIA',
        ];
        foreach (range(1, 14) as $key) {
            $monitoringColumns = array_merge($monitoringColumns, $columns);
        }
        $sheet->fromArray($monitoringColumns, null, 'R8');
        $sheet->fromArray(['DIRECCION', 'FECHA DE CONTACTO', 'COORDENADA'], null, 'DZ8');
        $sheet->getStyle('P2')->applyFromArray([
            'alignment' => ['horizontal' => Alignment::HORIZONTAL_CENTER],
            'fill' => ['fillType' => Fill::FILL_SOLID, 'startColor' => ['rgb' => 'FFFF00']],
            'font' => ['bold' => true],
        ]);
        $sheet->getStyle('R2:R3')->applyFromArray([
            'fill' => ['fillType' => Fill::FILL_SOLID, 'startColor' => ['rgb' => 'FFFF00']],
        ]);

        $data = [];
        foreach ($request->input('contacts', []) as $iecContact) {
            $contactRow = [
                mb_strtoupper($userCity),
                mb_strtoupper($iecContact['relationship_type']),
                mb_strtoupper($iecContact['first_name']),
                mb_strtoupper($iecContact['middle_name']),
                mb_strtoupper($iecContact['last_name']),
                mb_strtoupper($iecContact['second_last_name']),
                $iecContact['identification_type'],
                $iecContact['identification'],
                $iecContact['age'],
                $iecContact['age_measure'] === 'year' ? 1 : ($iecContact['age_measure'] === 'month' ? 2 : 3),
                $iecContact['gender'],
                trim("{$iecContact['primary_phone']} {$iecContact['secondary_phone']}"),
                mb_strtoupper($iecContact['eps']),
                'NO',
                'NO',
                $iecContact['sample_date'] ? Carbon::parse($iecContact['sample_date'])->format($defaultDateFormat) : 'NO',
                mb_strtoupper($iecContact['sample_result']),
            ];
            $contactDateCarbon = Carbon::parse($iecContact['contact_date']);
            $diffInDays = $contactDateCarbon->diffInDays($iecCarbonDate);
            $diffInDays = $diffInDays > 14 ? 14 : ($diffInDays ?: 1);
            $symptomsIds = collect($iecContact['symptoms_ids'] ?? null)->map('intval');
            $asymptomatic = $symptomsIds->isEmpty();
            $hasFever = $symptomsIds->contains(function ($symptomId) {
                return in_array($symptomId, [Symptom::FEVER, Symptom::SHIVERS, Symptom::DIARRHEA, Symptom::HEADACHE, Symptom::GENERAL_UNREST]);
            });
            $hasCough = $symptomsIds->contains(Symptom::COUGH);
            $hasBreathingDifficulty = $symptomsIds->contains(function ($symptomId) {
                return in_array($symptomId, [Symptom::BREATHING_DIFFICULTY, Symptom::TACHYPNEA, Symptom::CHEST_PAIN]);
            });
            $hasOdinophagy = $symptomsIds->contains(function ($symptomId) {
                return in_array($symptomId, [Symptom::SORE_THROAT, Symptom::LOSS_OF_SMELL_AND_TASTE]);
            });
            $hasFatigue = $symptomsIds->contains(function ($symptomId) {
                return in_array($symptomId, [Symptom::NAUSEAS, Symptom::VOMIT, Symptom::MYALGIA, Symptom::ABDOMINAL_PAIN]);
            });
            for ($i = 1; $i <= 14; $i++) {
                if ($i !== $diffInDays) {
                    // Empty values
                    for ($j = 1; $j <= 8; $j++) {
                        $contactRow[] = '';
                    }
                    continue;
                }
                $contactRow[] = '' /*$iecDate*/;
                $contactRow[] = '' /*$iecTime*/;
                $contactRow[] = '' /*$asymptomatic ? 'SI' : 'NO'*/;
                $contactRow[] = '' /*$hasFever ? 'SI' : 'NO'*/;
                $contactRow[] = '' /*$hasCough ? 'SI' : 'NO'*/;
                $contactRow[] = '' /*$hasBreathingDifficulty ? 'SI' : 'NO'*/;
                $contactRow[] = '' /*$hasOdinophagy ? 'SI' : 'NO'*/;
                $contactRow[] = '' /*$hasFatigue ? 'SI' : 'NO'*/;
            }
            $contactRow[] = $iecContact['address'];
            $contactRow[] = $contactDateCarbon->format($defaultDateFormat);
            $data[] = $contactRow;
        }
        $sheet->fromArray($data, null, 'A9');
        $highestRowAndColumn = $sheet->getHighestRowAndColumn();
        $lastRow = $highestRowAndColumn['row'];
        $lastColumn = $highestRowAndColumn['column'];
        $sheet->getStyle("A9:{$lastColumn}$lastRow")->applyFromArray([
            'borders' => ['allBorders' => ['borderStyle' => Border::BORDER_THIN]],
        ]);

        header('Content-Description: File Transfer');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment; filename=\"{$request->input('contact.name')}.xlsx\"");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
    }
}
