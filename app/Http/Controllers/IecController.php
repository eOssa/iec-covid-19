<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\IecsRepositoryInterface;
use App\Http\Resources\IecResource;
use App\Models\{Comorbidity, Contact, Displacement, Hospitalization, Iec, Symptom};
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\{Arr, Str};

class IecController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, IecsRepositoryInterface $iecsRepository)
    {
        if (!$request->ajax()) {
            return view('iecs.index');
        }
        $paginator = $iecsRepository->paginate($request->input('page'), $request->input('per_page'));
        $items = new Collection($paginator->items());
        $items->load('contact');
        return IecResource::collection($items)->additional(['total' => $paginator->total()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $iec = [
            'nursing_note' => 'Usuari@ de __ años de edad, con antecedentes patológicos de ___, paciente con reporte positivo del día __ de Noviembre, se encuentra en domicilio, al día de hoy refiere que "inició síntomas el día ________, presentó: fiebre de 38.9º, dificultad respiratoria, dolor torácico, dolor de cabeza y malestar general, es independiente tiene su propio negocio, convive actualmente con su esposo e hijos, todos ellos presentan síntomas, niega viajes nacionales e internacionales, no frecuenta ningún lugar, pero su esposo si, el supermercado y el banco dice que allí puede haberse contagiado. Se le realizó la toma de la prueba el día ________ en el Laboratorio López Correa, Antígeno, le formularon acetaminofén, y se está realizando remedios caseros con moringa".',
        ];
        return view('iecs.create', compact('iec'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /** @var \App\Models\Contact $contact */
        $contact = Contact::create($request->input('contact', []));
        $pregnancyWeeks = $request->input('pregnancy_weeks');
        $contact->comorbidities()->attach($this->getComorbiditiesIds($request->input('comorbidities', []), $pregnancyWeeks));
        $contact->displacements()->createMany($request->input('displacements', []));
        $contact->hospitalizations()->createMany($request->input('hospitalizations', []));
        $contact->symptoms()->attach($this->getSymptomsIds($request->input('symptoms', [])));

        $attributes = $request->input('iec', []);
        $attributes['medication'] = join(',', $attributes['medication'] ?? []);
        /** @var \App\Models\Iec $iec */
        $iec = $contact->iecs()->save(new Iec($attributes));
        foreach ((array) $request->input('contacts', []) as $contact) {
            $availableKeys = [
                'name',
                'eps',
                'identification_type_id',
                'identification',
                'gender',
                'address',
                'primary_phone',
            ];
            $iecContact = new Contact(Arr::only($contact, $availableKeys));
            $iecContact->birthdate = now()->sub($contact['age_measure'], $contact['age']);
            $iecContact->save();
            $iecContact->comorbidities()->attach($this->getComorbiditiesIds($contact['comorbidities'] ?? []));
            $iecContact->symptoms()->attach($this->getSymptomsIds($contact['symptoms'] ?? []));
            $attributes = [
                'relationship' => $contact['relationship'],
                'contact_date' => $contact['contact_date'],
                'sample_date' => $contact['sample_date'],
                'sample_result' => $contact['sample_result'],
            ];
            $iec->contacts()->attach($iecContact, $attributes);
        }
        if ($request->ajax()) {
            return response()->json(['id' => $iec->getKey()]);
        }
        return redirect()->route('iecs.edit', $iec);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Iec  $iec
     * @return \Illuminate\Http\Response
     */
    public function show(Iec $iec)
    {
        return new IecResource($iec);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Iec  $iec
     * @return \Illuminate\Http\Response
     */
    public function edit(Iec $iec)
    {
        $contact = $iec->getContact();
        $iec = [
            'contact' => [
                'address' => $contact->getAddress(),
                'birthdate' => $contact->getBirthdate()->toDateString(),
                'city' => $contact->getCity(),
                'comorbidities' => $contact->getComorbidities()->map->getKey()->all(),
                'displacements' => $contact->getDisplacements()->transform(function (Displacement $displacement) {
                    return [
                        'city' => $displacement->getCity(),
                        'country' => $displacement->getCountry(),
                        'endDate' => $displacement->getEndDate()->toDateString(),
                        'id' => $displacement->getKey(),
                        'startDate' => $displacement->getStartDate()->toDateString(),
                    ];
                }),
                'eps' => $contact->getEps(),
                'gender' => $contact->getGender(),
                'hospitalizations' => $contact->getHospitalizations()
                    ->transform(function (Hospitalization $hospitalization) {
                        return [
                            'date' => $hospitalization->getDate()->toDateString(),
                            'id' => $hospitalization->getKey(),
                            'institution' => $hospitalization->getInstitution(),
                        ];
                    }),
                'id' => $contact->getKey(),
                'identification' => $contact->getIdentification(),
                'identification_type_id' => $contact->getIdentificationTypeId(),
                'name' => $contact->getName(),
                'nationality' => $contact->getNationality(),
                'neighborhood' => $contact->getNeighborhood(),
                'occupation' => $contact->getOccupation(),
                'pregnancy_weeks' => $contact->getPregnancyWeeks(),
                'primary_phone' => $contact->getPrimaryPhone(),
                'region' => $contact->getRegion(),
                'secondary_phone' => $contact->getSecondaryPhone(),
                'symptoms' => $contact->getSymptoms()->map->getKey()->all(),
                'zone' => $contact->getZone(),
            ],
            'contacts' => $iec->getContacts()->transform(function (Contact $contact) {
                return [
                    'address' => $contact->getAddress(),
                    'age' => $contact->getAge(),
                    'ageMeasure' => $contact->getAgeMessure(),
                    'comorbidities' => $contact->getComorbidities()->map->getKey()->all(),
                    'contactDate' => $contact->pivot->getContactDate()->toDateString(),
                    'eps' => $contact->getEps(),
                    'gender' => $contact->getGender(),
                    'id' => $contact->getKey(),
                    'identification' => $contact->getIdentification(),
                    'identificationTypeId' => $contact->getIdentificationTypeId(),
                    'name' => $contact->getName(),
                    'primaryPhone' => $contact->getPrimaryPhone(),
                    'relationship' => $contact->pivot->getRelationship(),
                    'sampleDate' => $contact->pivot->getSampleDateString(),
                    'sampleResult' => $contact->pivot->getSampleResult(),
                    'symptoms' => $contact->getSymptoms()->map->getKey()->all(),
                ];
            }),
            'date' => $iec->getDate()->toDateString(),
            'exam_id' => $iec->getExamId(),
            'id' => $iec->getKey(),
            'medication' => $iec->getMedications(),
            'nursing_note' => $iec->getNursingNote(),
            'positive_contact_place' => $iec->getPositiveContactPlace(),
            'sample_date' => $iec->getSampleDate()->toDateString(),
            'sample_laboratory' => $iec->getSampleLaboratory(),
            'sample_type_id' => $iec->getSampleTypeId(),
            'symptom_date' => $iec->getSymptomDate()->toDateString(),
            'with_positive_contact' => $iec->withPositiveContact(),
        ];
        return view('iecs.create', compact('iec'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Iec  $iec
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Iec $iec)
    {
        $contact = $iec->getContact()->fill($request->input('contact', []));
        $contact->save();
        $pregnancyWeeks = $request->input('pregnancy_weeks');
        $contact->comorbidities()->sync($this->getComorbiditiesIds($request->input('comorbidities', []), $pregnancyWeeks));
        foreach (Arr::wrap($request->input('displacements')) as $displacement) {
            $contact->displacements()->updateOrCreate(['id' => $displacement['id'] ?? 0], $displacement);
        }
        foreach (Arr::wrap($request->input('hospitalizations')) as $hospitalization) {
            $contact->hospitalizations()->updateOrCreate(['id' => $hospitalization['id'] ?? 0], $hospitalization);
        }
        $contact->symptoms()->sync($this->getSymptomsIds($request->input('symptoms', [])));

        $attributes = $request->input('iec', []);
        $attributes['medication'] = join(',', $attributes['medication'] ?? []);
        $iec->fill($attributes);
        $iec->save();
        $iecContacts = [];
        foreach ((array) $request->input('contacts', []) as $contact) {
            $availableKeys = [
                'name',
                'eps',
                'identification_type_id',
                'identification',
                'gender',
                'address',
                'primary_phone',
            ];
            $iecContact = $iec->contacts()->findOrNew($contact['id'] ?? 0);
            /** @var \App\Models\Contact $iecContact */
            $iecContact->fill(Arr::only($contact, $availableKeys));
            $iecContact->birthdate = $iecContact->getBirthdate()->year(now()->sub($contact['age_measure'], $contact['age'])->year);
            $iecContact->save();
            $iecContact->comorbidities()->sync($this->getComorbiditiesIds($contact['comorbidities'] ?? [], $iecContact->getPregnancyWeeks()));
            $iecContact->symptoms()->sync($this->getSymptomsIds($contact['symptoms'] ?? []));
            $attributes = [
                'relationship' => $contact['relationship'],
                'contact_date' => $contact['contact_date'],
                'sample_date' => $contact['sample_date'],
                'sample_result' => $contact['sample_result'],
            ];
            $iecContacts[$iecContact->getKey()] = $attributes;
        }
        $iec->contacts()->sync($iecContacts);
        if ($request->ajax()) {
            return response()->json(['id' => $iec->getKey()]);
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Iec  $iec
     * @return \Illuminate\Http\Response
     */
    public function destroy(Iec $iec)
    {
        //
    }

    private function getComorbiditiesIds($comorbiditiesInput, $pregnancyWeeks = 0)
    {
        $comorbiditiesIds = array_filter($comorbiditiesInput, 'is_numeric');
        $newComorbiditiesIds = array_map(function ($comorbidity) {
            if ($model = Comorbidity::whereName($comorbidity)->first()) {
                return $model->getKey();
            }
            return Comorbidity::create(['name' => $comorbidity])->getKey();
        }, array_values(array_diff($comorbiditiesInput, $comorbiditiesIds)));
        return collect(array_merge($comorbiditiesIds, $newComorbiditiesIds))
            ->mapWithKeys(function ($comorbidityId) use ($pregnancyWeeks) {
                $comorbidityId = (int) $comorbidityId;
                if ($comorbidityId === Comorbidity::PREGNANCY) {
                    return [$comorbidityId => ['additional' => $pregnancyWeeks]];
                }
                return [$comorbidityId => []];
            });
    }

    private function getSymptomsIds($symptomsInput)
    {
        $symptomsIds = array_filter($symptomsInput, 'is_numeric');
        $newSymptomsIds = array_map(function ($symptom) {
            if ($model = Symptom::whereName($symptom)->first()) {
                return $model->getKey();
            }
            return Symptom::create(['name' => $symptom])->getKey();
        }, array_values(array_diff($symptomsInput, $symptomsIds)));
        return array_merge($symptomsIds, $newSymptomsIds);
    }
}
