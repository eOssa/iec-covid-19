<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class IecFoldersReportController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->validate(['iecs' => ['required', 'array']]);
        $iecs = $request->input('iecs');
        $iecs = array_filter($iecs, function ($iec) {
            return $iec['iec']['is_effective'];
        });
        usort($iecs, function ($a, $b) {
            $ad = Carbon::parse($a['iec']['date']);
            $bd = Carbon::parse($b['iec']['date']);
            if ($ad->eq($bd)) {
                return 0;
            }
            return $ad->lt($bd) ? -1 : 1;
        });
        $zip = new \ZipArchive();
        $filename = 'Informe ' . now()->toDateString() . '.zip';
        $zip_file = storage_path("app/$filename");
        if ($zip->open($zip_file, \ZipArchive::CREATE) !== true) {
            throw new \Exception('Could not create zip file');
        }
        $folder = 'INVESTIGACIONES';
        $zip->addEmptyDir($folder);
        foreach ($iecs as $i => $iec) {
            $index = $i + 1;
            $response = Http::acceptJson()->post(url('/api/iecs/report'), $iec);
            $word = $response->body();
            $wordFilename = Str::after($response->header('Content-Disposition'), 'filename="');
            $wordFilename = Str::before($wordFilename, '"');
            $iecDateCarbon = Carbon::parse($iec['iec']['date']);
            $subfolder = "$folder/$index. {$iecDateCarbon->format('d-m-y')} POSITIVO";
            $zip->addFromString("$subfolder/$wordFilename", $word);
            if (is_array($iec['iecContacts']['contacts']) && count($iec['iecContacts']['contacts']) > 0) {
                $response = Http::acceptJson()->post(url('api/iecs/contacts/report'), $iec['iecContacts']);
                $excel = $response->body();
                $excelFilename = Str::after($response->header('Content-Disposition'), 'filename="');
                $excelFilename = Str::before($excelFilename, '"');
                $zip->addFromString("$subfolder/$excelFilename", $excel);
            }
        }
        $zip->close();
        return response()->download($zip_file)->deleteFileAfterSend(true);
    }
}
