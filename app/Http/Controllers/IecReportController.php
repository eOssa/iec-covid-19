<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\{
    ComorbiditiesRepositoryInterface,
    ExamsRepositoryInterface,
    SamplesTypesRepositoryInterface,
    SymptomsRepositoryInterface
};
use App\Models\Exam;
use Facades\App\Models\{Comorbidity as ComorbidityFacade, SampleType as SampleTypeFacade, Symptom as SymptomFacade};
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use PhpOffice\PhpWord\{IOFactory, PhpWord};
use PhpOffice\PhpWord\Shared\Converter;
use PhpOffice\PhpWord\SimpleType\Jc;
use PhpOffice\PhpWord\Style\{Font, Language};

class IecReportController extends Controller
{
    public function __invoke(
        Request $request,
        ComorbiditiesRepositoryInterface $comorbiditiesRepository,
        ExamsRepositoryInterface $examsRepository,
        SamplesTypesRepositoryInterface $samplesTypesRepository,
        SymptomsRepositoryInterface $symptomsRepository
    ) {
        $request->validate([
            'contact.address' => ['nullable', 'string'],
            'contact.birthdate' => ['nullable', 'date'],
            'contact.city' => ['required', 'string'],
            'contact.comorbidities.*.id' => ['required', 'integer'],
            'contact.comorbidities.*.name' => ['required', 'string'],
            'contact.displacements' => ['nullable', 'array'],
            'contact.displacements.*.city' => ['required', 'string'],
            'contact.displacements.*.country' => ['required', 'string'],
            'contact.displacements.*.end_date' => ['required', 'date'],
            'contact.displacements.*.start_date' => ['required', 'date'],
            'contact.eps' => ['nullable', 'string'],
            'contact.hospitalizations' => ['nullable', 'array'],
            'contact.hospitalizations.*.date' => ['required', 'date'],
            'contact.hospitalizations.*.institution' => ['required', 'string'],
            'contact.identification' => ['required', 'string'],
            'contact.identification_type' => ['required', 'string'],
            'contact.is_female' => ['required', 'boolean'],
            'contact.is_male' => ['required', 'boolean'],
            'contact.live_in_urban' => ['required', 'boolean'],
            'contact.live_in_rural' => ['required', 'boolean'],
            'contact.name' => ['required', 'string'],
            'contact.nationality' => ['nullable', 'string'],
            'contact.neighborhood' => ['nullable', 'string'],
            'contact.occupation' => ['nullable', 'string'],
            'contact.primary_phone' => ['required', 'string'],
            'contact.pregnancy_weeks' => ['required', 'integer'],
            'contact.region' => ['required', 'string'],
            'contact.secondary_phone' => ['nullable', 'string'],
            'contact.symptoms.*.id' => ['required', 'integer'],
            'contact.symptoms.*.name' => ['required', 'string'],
            'contacts.*.address' => ['required', 'string'],
            'contacts.*.age_with_measure' => ['required', 'string'],
            'contacts.*.comorbidities' => ['nullable', 'string'],
            'contacts.*.contact_date' => ['required', 'date'],
            'contacts.*.eps' => ['required', 'string'],
            'contacts.*.identification' => ['required', 'string'],
            'contacts.*.identification_type' => ['required', 'string'],
            'contacts.*.name' => ['required', 'string'],
            'contacts.*.primary_phone' => ['required', 'string'],
            'contacts.*.relationship' => ['required', 'string'],
            'contacts.*.sample_date' => ['nullable', 'string'],
            'contacts.*.sample_result' => ['nullable', 'string'],
            'contacts.*.symptoms' => ['nullable', 'string'],
            'iec.date' => ['required', 'date'],
            'iec.exam_id' => ['required', 'integer'],
            'iec.is_effective' => ['nullable', 'boolean'],
            'iec.medications' => ['nullable', 'string'],
            'iec.nursing_note' => ['nullable', 'string'],
            'iec.positive_contact_place' => ['nullable', 'string'],
            'iec.sample_date' => ['nullable', 'date'],
            'iec.sample_laboratory' => ['nullable', 'string'],
            'iec.sample_type_id' => ['nullable', 'integer'],
            'iec.symptom_date' => ['nullable', 'date'],
            'iec.with_positive_contact' => ['required', 'boolean'],
            'user.city' => ['required', 'string'],
            'user.name' => ['required', 'string'],
            'user.phone' => ['required', 'string'],
            'user.region' => ['required', 'string'],
        ]);
        $iec = $request->input('iec');
        $iecDateCarbon = Carbon::parse($iec['date']);
        $contact = $request->input('contact');
        $contactDisplacements = collect($contact['displacements'] ?? null);
        $contactBirthdateCarbon = Carbon::parse($contact['birthdate']);
        $isEffective = ($iec['is_effective'] ?? '1') == '1';
        $phpWord = new PhpWord();
        $phpWord->getDocInfo()->setCreator('DanefSoft');
        $defaultDateFormat = 'd-m-y';
        $slashDateFormat = 'd/m/y';
        $cellTitleHeight = Converter::cmToTwip(8.31);
        $contactBirthdate = $contact['birthdate'] ? $contactBirthdateCarbon->format($defaultDateFormat) : null;
        $iecSymptomDate = $iec['symptom_date'] ? Carbon::parse($iec['symptom_date'])->format($defaultDateFormat) : null;
        $contactAge = $contact['birthdate'] ? $contactBirthdateCarbon->age : null;

        $phpWord->setDefaultFontName('Arial');
        $phpWord->setDefaultFontSize(11);
        $phpWord->getSettings()->setHideGrammaticalErrors(true);
        $phpWord->getSettings()->setThemeFontLang(new Language(Language::ES_ES));
        $phpWord->addFontStyle('BoxTitle', ['size' => 8, 'bold' => true]);
        $phpWord->addFontStyle('Glossary', ['size' => 9]);
        $phpWord->addFontStyle('Bold', ['bold' => true]);
        $phpWord->addFontStyle('UnderlineDot', ['underline' => Font::UNDERLINE_DOTTED]);
        $phpWord->addParagraphStyle('Center', ['alignment' => Jc::CENTER]);
        $phpWord->addTableStyle('TitleTable', ['borderColor' => '000000', 'bgColor' => 'BEBEBE', 'borderSize'  => 1, 'cellMargin' => 80]);
        $phpWord->addTableStyle('Table', ['borderColor' => '000000', 'borderSize'  => 1, 'alignment' => Jc::CENTER, 'cellMarginLeft' => 80, 'cellMarginRight' => 80]);
        $phpWord->addFontStyle('ConfirmationCell', ['name' => 'Times New Roman', 'size' => 10, 'alignment' => Jc::CENTER]);

        $section = $phpWord->addSection();
        $section->addHeader()->addImage(base_path('resources/images/Header.png'), ['width' => Converter::cmToPoint(12.9), 'height' => Converter::cmToPoint(2.46), 'alignment' => Jc::CENTER]);
        $section->addFooter()->addImage(base_path('resources/images/Footer.png'), ['width' => Converter::cmToPoint(13.16), 'height' => Converter::cmToPoint(1.7), 'alignment' => Jc::CENTER]);
        $section->addText('* Contacto estrecho:', ['size' => 10, 'color' => '006FC0', 'bold' => true]);
        $section->addTextBreak();
        $section->addText('- La persona que se encuentra menos de 2 metros de un caso confirmado de COVID-19. Este contacto puede ocurrir mientras cuida, viva, visite, comparta un área de espera, se encuentra en el lugar de trabajo o en reuniones con un caso de COVID-19.', 'Glossary');
        $section->addText('- Una persona que tenga contacto directo, sin protección, con secreciones infecciosas de un caso de COVID-19 (por ejemplo, con la tos o la manipulación de los pañuelos utilizados).', 'Glossary');
        $section->addText('- Un trabajador del ámbito hospitalario que tenga contacto con caso probable o confirmado de COVID-19', 'Glossary');
        $section->addText('- Una persona que viaje en cualquier tipo de transporte y se siente dos asientos, en cualquier dirección, del caso de COVID-19. Los contactos incluyen compañeros de viaje y personal de la tripulación que brinde atención al caso durante el viaje.', 'Glossary');
        $section->addTextBreak();
        $section->addText("Fecha de investigación: {$iecDateCarbon->format($defaultDateFormat)}\t\t\t\tEPS: {$contact['eps']}");
        $section->addTextBreak();

        $section->addTable('TitleTable')->addRow()->addCell($cellTitleHeight)->addText('DATOS GENERALES DEL PACIENTE', 'BoxTitle');
        $section->addTextBreak();
        $section->addText("Nombres y apellidos: {$contact['name']}");
        $section->addText("Tipo de documento: {$contact['identification_type']}\t\t\tDocumento identidad: {$contact['identification']}");
        $section->addText("Fecha nacimiento: $contactBirthdate\t\t\tEdad: $contactAge\t\tSexo: F " . ($contact['is_female'] ? 'X' : '___') . ' M ' . ($contact['is_male'] ? 'X' : '___'));
        $section->addText("Nacionalidad: {$contact['nationality']}");
        $section->addText("Domicilio: {$contact['address']}");
        $section->addText("Residencia: Departamento: {$contact['region']}\tMunicipio: {$contact['city']}\tBarrio: {$contact['neighborhood']}");
        $section->addTextBreak();
        $section->addText("Dirección: {$contact['address']}");
        $section->addTextBreak();
        $section->addText("Zona de residencia: Urbana " . ($isEffective && $contact['live_in_urban'] ? 'X' : '___') . ' Rural ' . ($isEffective && $contact['live_in_rural'] ? 'X' : '___'));
        $section->addText("Tel. de contacto 1: {$contact['primary_phone']} Tel. de contacto 2: {$contact['secondary_phone']}");
        $section->addText("Ocupación: {$contact['occupation']}");
        $section->addTextBreak();

        $section->addTable('TitleTable')->addRow()->addCell($cellTitleHeight)->addText('ANTECEDENTES DE RIESGO Y EXPOSICIÓN', 'BoxTitle');
        $section->addTextBreak();
        $section->addText("Fecha de inicio de síntomas: $iecSymptomDate");
        $section->addTextBreak();
        $section->addText("Desplazamientos en los últimos 14 días: Si " . ($isEffective && $contactDisplacements->isNotEmpty() ? 'X' : '___') . '  No ' . ($isEffective && $contactDisplacements->isEmpty() ? 'X' : '___'));
        $section->addTextBreak();
        foreach ($contactDisplacements as $key => $displacement) {
            $number = $key + 1;
            $displacementStartDateCarbon = Carbon::parse($displacement['start_date']);
            $displacementEndDateCarbon = Carbon::parse($displacement['end_date']);
            $section->addText("País $number: {$displacement['country']}\tCiudad: {$displacement['city']}\tPeriodo de estadía: del {$displacementStartDateCarbon->format($slashDateFormat)} al {$displacementEndDateCarbon->format($slashDateFormat)}");
            $section->addTextBreak();
        }
        for ($i = $contactDisplacements->count() + 1; $i <= 6; $i++) {
            $textRun = $section->addTextRun();
            $textRun->addText("País $i:");
            $textRun->addText("\t\t\t", 'UnderlineDot');
            $textRun->addText("Ciudad:");
            $textRun->addText("\t\t", 'UnderlineDot');
            $textRun->addText("Periodo de estadía: del");
            $textRun->addText("     ", 'UnderlineDot');
            $textRun->addText("/", 'UnderlineDot');
            $textRun->addText("     ", 'UnderlineDot');
            $textRun->addText("/", 'UnderlineDot');
            $textRun->addText("     ", 'UnderlineDot');
            $textRun->addText("al");
            $textRun->addText("     ", 'UnderlineDot');
            $textRun->addText("/", 'UnderlineDot');
            $textRun->addText("     ", 'UnderlineDot');
            $textRun->addText("/", 'UnderlineDot');
            $textRun->addText("    .", 'UnderlineDot');
            $section->addTextBreak();
        }
        $section->addPageBreak();

        $section = $phpWord->addSection();
        $section->addText('¿Tuvo contacto cercano con un caso confirmado o probable de infección por COVID-19?:');
        $section->addTextBreak();
        $section->addText("Si " . ($isEffective && $iec['with_positive_contact'] ? 'X' : '___') . " No " . ($isEffective && !$iec['with_positive_contact'] ? 'X' : '___') . "\tLugar: {$iec['positive_contact_place']}");
        $section->addTextBreak();

        $section->addTable('TitleTable')->addRow()->addCell($cellTitleHeight)->addText('ANTECEDENTES CLÍNICOS Y DE HOSPITALIZACIÓN', 'BoxTitle');
        $section->addTextBreak();
        $contactHospitalizations = collect($contact['hospitalizations'] ?? null);
        foreach ($contactHospitalizations as $key => $hospitalization) {
            $number = $key + 1;
            $hospitalizationDateCarbon = Carbon::parse($hospitalization['date']);
            $section->addText("Fecha {$number}° consulta: {$hospitalizationDateCarbon->format($slashDateFormat)}\t\tInstitución de salud: {$hospitalization['institution']}");
            $section->addTextBreak();
        }
        for ($i = $contactHospitalizations->count() + 1; $i <= 6; $i++) {
            $textRun = $section->addTextRun();
            $textRun->addText("Fecha {$i}° consulta:");
            $textRun->addText("     ", 'UnderlineDot');
            $textRun->addText("/", 'UnderlineDot');
            $textRun->addText("     ", 'UnderlineDot');
            $textRun->addText("/", 'UnderlineDot');
            $textRun->addText("     ", 'UnderlineDot');
            $textRun->addText("\tInstitución de salud:");
            $textRun->addText("\t\t\t\t\t", 'UnderlineDot');
            $section->addTextBreak();
        }
        $section->addText('Signos y síntomas al ingreso y días previos:');
        $section->addTextBreak();
        $medications = $iec['medications'] ?? '';
        $section->addText('¿Está consumiendo medicamentos antiinflamatorios o acetaminofén? Si ' . ($isEffective && $medications ? 'X' : '___') . ' No ' . ($isEffective && !$medications ? 'X' : '___'))->setFontStyle(['color' => 'red']);
        $section->addText($medications);
        $section->addTextBreak();

        $table = $section->addTable('Table');
        $symptoms = collect($contact['symptoms'] ?? null);
        $orderedSymptomsIds = [[1, 9], [2, 10], [3, 11], [4, 12], [5, 13], [6, 14], [7, 'Otro'], [8, '¿Cual?']];
        $symptomsCollection = $symptomsRepository->all();
        foreach ($orderedSymptomsIds as $chunk) {
            $table->addRow(Converter::cmToTwip(0.44));
            foreach ($chunk as $symptomId) {
                if (!in_array($symptomId, ['Otro', '¿Cual?'])) {
                    /** @var \App\Models\Symptom $symptom */
                    $symptom = $symptomsCollection->firstWhere(SymptomFacade::getKeyName(), $symptomId);
                    $table->addCell(Converter::cmToTwip(4.98))->addText($symptom->getName());
                    $table->addCell(Converter::cmToTwip(0.53))->addText($symptoms->pluck('id')->contains($symptom->getKey()) ? 'SI' : 'NO', 'ConfirmationCell', 'Center');
                    continue;
                }
                $cell = $table->addCell(Converter::cmToTwip(4.98));
                $otherSymptoms = $symptoms->where('id', '>', 14);
                $cell->addText($symptomId);
                if ($symptomId === '¿Cual?') {
                    $cell->addText($otherSymptoms->isNotEmpty() ? $otherSymptoms->implode('name', ', ') : '');
                }
                $hasOthers = $otherSymptoms->isNotEmpty() ? 'SI' : 'NO';
                $table->addCell(Converter::cmToTwip(0.53))->addText($symptomId === '¿Cual?' ? '' : $hasOthers, 'ConfirmationCell', 'Center');
            }
        }
        $section->addTextBreak();

        $section->addTable('TitleTable')->addRow()->addCell($cellTitleHeight)->addText('COMORBILIDADES/ FACTORES DE RIESGO', 'BoxTitle');
        $section->addTextBreak();
        $table = $section->addTable('Table');
        $comorbidities = collect($contact['comorbidities'] ?? null);
        $comorbiditiesIds = $comorbidities->pluck('id');
        $orderedComorbiditiesIds = [[1, 8], [2, 9], [3, 10], [4, 11], [5, 12], [6, 13], [7, 14]];
        $comorbiditiesCollection = $comorbiditiesRepository->all();
        foreach ($orderedComorbiditiesIds as $chunk) {
            $table->addRow(Converter::cmToTwip(0.51));
            foreach ($chunk as $comorbidityId) {
                /** @var \App\Models\Comorbidity $comorbidity */
                $comorbidity = $comorbiditiesCollection->firstWhere(ComorbidityFacade::getKeyName(), $comorbidityId);
                $text = $comorbidity->getName();
                if ($comorbidity->isPregnancy()) {
                    $text .= ", Semanas de gestación: " . ($comorbiditiesIds->contains($comorbidity->getKey()) ? (int) $contact['pregnancy_weeks'] : '__');
                }
                $table->addCell(Converter::cmToTwip(7.83))->addText($text, null, 'Center');
                $table->addCell(Converter::cmToTwip(1.19))
                    ->addText($comorbiditiesIds->contains($comorbidity->getKey()) ? 'SI' : 'NO', 'ConfirmationCell', 'Center');
            }
        }
        $otherComorbidities = $comorbidities->where('id', '>', 14);
        $section->addText($otherComorbidities->isNotEmpty() ? $otherComorbidities->implode('name', ', ') : '');

        $section->addTable('TitleTable')->addRow()->addCell($cellTitleHeight)->addText('DATOS DE LABORATORIO', 'BoxTitle');
        $section->addTextBreak();
        $section->addText('Laboratorio para diagnóstico etiológico', 'Bold');
        $section->addTextBreak();
        $iecSampleDateCarbon = $iec['sample_date'] ? Carbon::parse($iec['sample_date'])->format($defaultDateFormat) : null;
        $section->addText("Fecha de toma de primera muestra: $iecSampleDateCarbon {$iec['sample_laboratory']}");
        $section->addPageBreak();

        $section = $phpWord->addSection();
        $section->addText("Tipo de muestra:");
        $orderedSamplesTypes = [[1, 3], [2, 4]];
        $sampleTypesCollection = $samplesTypesRepository->all();
        foreach ($orderedSamplesTypes as $chunk) {
            $textRun = $section->addTextRun();
            foreach ($chunk as $key => $sampleTypeId) {
                /** @var \App\Models\SampleType $sampleType */
                $sampleType = $sampleTypesCollection->firstWhere(SampleTypeFacade::getKeyName(), $sampleTypeId);
                $textRun->addText($sampleType->getName());
                $textRun->addText(str_repeat("\t", mb_strlen($sampleType->getName()) < 19 ? 2 : 1), 'UnderlineDot');
                $textRun->addText(($isEffective && $sampleType->getKey() === (int) $iec['sample_type_id'] ? 'X' : '') . "\t", 'UnderlineDot');
            }
        }
        $textRun = $section->addTextRun();
        $textRun->addText('Otro');
        $textRun->addText("\t\t\t\t\t", 'UnderlineDot');
        $textRun->addText('Cual');
        $textRun->addText("\t\t\t\t\t", 'UnderlineDot');
        $section->addTextBreak(2);
        $section->addText('Resultado:', 'Bold');
        foreach ($examsRepository->all() as $exam) {
            /** @var \App\Models\Exam $exam */
            $name = $exam->getKey() === Exam::ANTIGEN ? 'Film Array' : $exam->getName();
            $textRun = $section->addTextRun();
            $textRun->addText($name);
            $textRun->addText(str_repeat("\t", $exam->getKey() === Exam::ANTIGEN ? 2 : 3) . ($isEffective && $exam->getKey() === (int) $iec['exam_id'] ? 'X' : ''), 'UnderlineDot');
            $textRun->addText("\t\t", 'UnderlineDot');
        }
        $section->addTextBreak();

        $table = $section->addTable('Table');
        $table->addRow(Converter::cmToTwip(0.81));
        $cell = $table->addCell(Converter::cmToTwip(19.79));
        $cell->addText('Entrevista con el paciente, familiares e informantes clave,', 'Bold', 'Center');
        $cell->addText('complementar con datos de la historia clínica', 'Bold', 'Center');
        $table->addRow(Converter::cmToTwip(0.81));
        $cell = $table->addCell(Converter::cmToTwip(19.79));
        $cell->addListItem('Relato de la progresión de la enfermedad actual.', 0, ['size' => 10], null, 'Center');
        $cell->addTextBreak();
        $cell->addListItem('Si el paciente es procedente del exterior incluir itinerario de viaje, actividades realizadas durante el viaje ubicación en el avión, número y lugar de escalas hacer énfasis en la fecha de Cada desplazamiento etc.', 0, ['size' => 10], null, 'Center');
        $cell->addTextBreak();
        $cell->addListItem('Identificar lugares visitados desde el inicio de los síntomas (identifique actividades realizadas, sitios de tránsito, medios de transporte y otros factores de riesgo)', 0, ['size' => 10], null, 'Center');
        $cell->addTextBreak(2);
        $cell->addText($iec['nursing_note']);
        $cell->addTextBreak(4);
        $rowHeight = Converter::cmToTwip(0.47);
        $cellWidth = Converter::cmToTwip(4.72);
        $cellInfoWidth = Converter::cmToTwip(11.77);
        $numSymptomatic = 0;
        $contacts = collect($request->input('contacts'));
        foreach ($contacts as $iecContact) {
            $nestedTable = $cell->addTable('Table');
            $nestedTable->addRow($rowHeight);
            $nestedTable->addCell($cellWidth)->addText('NOMBRE', 'Bold');
            $nestedTable->addCell($cellInfoWidth)->addText($iecContact['name']);
            $nestedTable->addRow($rowHeight);
            $nestedTable->addCell($cellWidth)->addText($iecContact['identification_type'], 'Bold');
            $nestedTable->addCell($cellInfoWidth)->addText($iecContact['identification']);
            $nestedTable->addRow($rowHeight);
            $nestedTable->addCell($cellWidth)->addText('EPS', 'Bold');
            $nestedTable->addCell($cellInfoWidth)->addText($iecContact['eps']);
            $nestedTable->addRow($rowHeight);
            $nestedTable->addCell($cellWidth)->addText('TEL', 'Bold');
            $nestedTable->addCell($cellInfoWidth)->addText($iecContact['primary_phone']);
            $nestedTable->addRow($rowHeight);
            $nestedTable->addCell($cellWidth)->addText('FC POSITIVO', 'Bold');
            $nestedTable->addCell($cellInfoWidth)->addText(Carbon::parse($iecContact['contact_date'])->format($defaultDateFormat));
            $nestedTable->addRow($rowHeight);
            $nestedTable->addCell($cellWidth)->addText('EDAD', 'Bold');
            $nestedTable->addCell($cellInfoWidth)->addText(mb_strtoupper($iecContact['age_with_measure']));
            $nestedTable->addRow($rowHeight);
            $nestedTable->addCell($cellWidth)->addText('SÍNTOMAS', 'Bold');
            $nestedTable->addCell($cellInfoWidth)->addText(($iecContact['symptoms'] ?? null) ?: 'NINGUNO');
            $nestedTable->addRow($rowHeight);
            $nestedTable->addCell($cellWidth)->addText('DIRECCION', 'Bold');
            $nestedTable->addCell($cellInfoWidth)->addText($iecContact['address']);
            $nestedTable->addRow($rowHeight);
            $nestedTable->addCell($cellWidth)->addText('PARENTESCO', 'Bold');
            $nestedTable->addCell($cellInfoWidth)->addText($iecContact['relationship']);
            $nestedTable->addRow($rowHeight);
            $nestedTable->addCell($cellWidth)->addText('ANTECEDENTES', 'Bold');
            $fcText = '';
            if ($iecContact['sample_date'] ?? null) {
                $sampleDateCarbon = Carbon::parse($iecContact['sample_date']);
                $fcText = "   FM {$sampleDateCarbon->format($defaultDateFormat)} {$iecContact['sample_result']}";
            }
            $nestedTable->addCell($cellInfoWidth)->addText(trim(($iecContact['comorbidities'] ?? null) . $fcText ?: 'NIEGA'));
            $cell->addTextBreak(3);
            if (collect($iecContact['symptoms'] ?? null)->isNotEmpty()) {
                $numSymptomatic++;
            }
        }
        if ($contacts->isNotEmpty()) {
            $numSymptomatic = $numSymptomatic ?: 'Ninguno';
            $cell->addText("Se relaciona {$contacts->count()} contacto" . ($contacts->count() === 1 ? '' : 's') . ", $numSymptomatic de ellos sintomático" . ($numSymptomatic === 1 ? '' : 's') . '.', 'Bold');
            $cell->addTextBreak();
        }
        if ($isEffective) {
            $cell->addText('Se le brinda educación al paciente relacionado a todos los componentes del aislamiento preventivo en el hogar tales como:');
            $cell->addTextBreak();
            $cell->addText('Estar en aislamiento preventivo por 7 días a partir del inicio de los síntomas, cada habitante del hogar debe permanecer en habitaciones separadas y limitar el desplazamiento en el interior de ella, lavar muy bien los utensilios como sabanas, toalla, cobijas, y platos después de uso; las mascarilla solo deben ser usadas por mayores a 3 años, realizar apertura periódica de las ventanas, mantener una distancia mínima de un metro con el enfermo dormir en cama separada, no recibir visita en el hogar, la persona que se encuentre en buenas condiciones de salud se recomienda encargarse de las actividades domésticas. En lo posible la persona aislada deberá tener baño exclusivo, en caso de no poder contar con esto deberá realizar desinfección una vez lo use. Limitar y reducir el número de personas que socialicen con las personas en aislamiento, la persona enferma deberá utilizar mascarilla quirúrgica, los elementos como guantes y tapabocas deben ser eliminados en bolsas diferentes a la basura común, implementar rutinas de lavado frecuente de las manos con agua y jabón, esto disminuye en un 50% la posibilidad de infectarse. El lavado de manos debe ser de 40 a 60 segundos, en especial en los siguientes momentos: después de cualquier actividad física, cuando las manos están sucias visiblemente. No auto-medicarse y se explica en qué condiciones debe acudir por urgencias. El paciente entra a una fase de autocontrol y auto-vigilancia de los síntomas, en caso de tener sintomatologías leves comunicarse en su EPS para recibir una tele-consulta, se le explica al paciente la ruta para darle salida del aislamiento preventivo.');
            $cell->addTextBreak(5);
        }
        $section->addPageBreak();

        $table = $section->addTable('Table');
        $table->addRow()->addCell()->addText('Entrevista con el paciente, familiares e informantes clave, complementar con datos de la historia clínica', 'Bold', 'Center');
        $table->addRow(Converter::cmToTwip(16))->addCell();

        $section->addTextBreak();

        $section->addTable('TitleTable')->addRow()->addCell($cellTitleHeight)->addText('RESPONSABLE DE LA INVESTIGACIÓN', 'BoxTitle');
        $section->addTextBreak();
        $user = $request->input('user');
        $section->addText("Nombre y firma del entrevistador: $user[name]");
        $section->addText("Municipio: $user[city]\tDepartamento: $user[region]");
        $section->addText("Teléfono: $user[phone]");

        $file = "{$contact['name']}.docx";
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=\"$file\"");
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');
        $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('php://output');
    }
}
