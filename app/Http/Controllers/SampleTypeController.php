<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\SamplesTypesRepositoryInterface;
use App\Http\Resources\SampleTypeResource;
use App\Models\SampleType;
use Illuminate\Http\Request;

class SampleTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SamplesTypesRepositoryInterface $samplesTypesRepository)
    {
        return SampleTypeResource::collection($samplesTypesRepository->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SampleType  $sampleType
     * @return \Illuminate\Http\Response
     */
    public function show(SampleType $sampleType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SampleType  $sampleType
     * @return \Illuminate\Http\Response
     */
    public function edit(SampleType $sampleType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SampleType  $sampleType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SampleType $sampleType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SampleType  $sampleType
     * @return \Illuminate\Http\Response
     */
    public function destroy(SampleType $sampleType)
    {
        //
    }
}
