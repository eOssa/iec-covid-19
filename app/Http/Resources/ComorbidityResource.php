<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ComorbidityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\Comorbidity $comorbidity */
        $comorbidity = $this;
        return [
            'id' => $comorbidity->getKey(),
            'name' => $comorbidity->getName(),
        ];
    }
}
