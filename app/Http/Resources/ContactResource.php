<?php

namespace App\Http\Resources;

use App\Models\Displacement;
use App\Models\Hospitalization;
use Illuminate\Http\Resources\Json\JsonResource;

class ContactResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\Contact $contact */
        $contact = $this;
        return [
            'address' => $contact->getAddress(),
            'age' => $contact->getAge(),
            'age_measure' => $contact->getAgeMessure(),
            'birthdate' => $contact->getBirthdate()->toDateString(),
            'city' => $contact->getCity(),
            'comorbidities' => $contact->getComorbidities()->map->getKey()->all(),
            'contact_date' => $contact->pivot ? $contact->pivot->getContactDate()->toDateString() : null,
            'displacements' => $contact->getDisplacements()->transform(function (Displacement $displacement) {
                return new DisplacementResource($displacement);
            }),
            'eps' => $contact->getEps(),
            'gender' => $contact->getGender(),
            'hospitalizations' => $contact->getHospitalizations()
                ->transform(function (Hospitalization $hospitalization) {
                    return new HospitalizationResource($hospitalization);
                }),
            'id' => $contact->getKey(),
            'identification' => $contact->getIdentification(),
            'identification_type_id' => $contact->getIdentificationTypeId(),
            'name' => $contact->getName(),
            'nationality' => $contact->getNationality(),
            'neighborhood' => $contact->getNeighborhood(),
            'occupation' => $contact->getOccupation(),
            'pregnancy_weeks' => $contact->getPregnancyWeeks(),
            'primary_phone' => $contact->getPrimaryPhone(),
            'region' => $contact->getRegion(),
            'relationship' => $contact->pivot ? $contact->pivot->getRelationship() : null,
            'sample_date' => $contact->pivot ? $contact->pivot->getSampleDateString() : null,
            'sample_result' => $contact->pivot ? $contact->pivot->getSampleResult() : null,
            'secondary_phone' => $contact->getSecondaryPhone(),
            'symptoms' => $contact->getSymptoms()->map->getKey()->all(),
            'zone' => $contact->getZone(),
        ];
    }
}
