<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DisplacementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\Displacement $displacement */
        $displacement = $this;
        return [
            'city' => $displacement->getCity(),
            'country' => $displacement->getCountry(),
            'end_date' => $displacement->getEndDate()->toDateString(),
            'id' => $displacement->getKey(),
            'start_date' => $displacement->getStartDate()->toDateString(),
        ];
    }
}
