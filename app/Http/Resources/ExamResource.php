<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ExamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\Exam $exam */
        $exam = $this;
        return [
            'id' => $exam->getKey(),
            'name' => $exam->getName(),
        ];
    }
}
