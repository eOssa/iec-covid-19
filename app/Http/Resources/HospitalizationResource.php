<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HospitalizationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\Hospitalization $hospitalization */
        $hospitalization = $this;
        return [
            'date' => $hospitalization->getDate()->toDateString(),
            'id' => $hospitalization->getKey(),
            'institution' => $hospitalization->getInstitution(),
        ];
    }
}
