<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IdentificationTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\IdentificationType $identificationType */
        $identificationType = $this;
        return [
            'id' => $identificationType->getKey(),
            'name' => $identificationType->getName(),
        ];
    }
}
