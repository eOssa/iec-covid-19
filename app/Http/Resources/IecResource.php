<?php

namespace App\Http\Resources;

use App\Models\Contact;
use Illuminate\Http\Resources\Json\JsonResource;

class IecResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\Iec $iec */
        $iec = $this;
        return [
            'id' => $iec->getKey(),
            'contact' => new ContactResource($iec->getContact()),
            'contacts' => $iec->getContacts()->transform(function (Contact $contact) {
                return new ContactResource($contact);
            }),
            'date' => $iec->getDate()->toDateString(),
            'exam_id' => $iec->getExamId(),
            'medications' => $iec->getMedications(),
            'name' => $iec->getContact()->getName(),
            'nursing_note' => $iec->getNursingNote(),
            'positive_contact_place' => $iec->getPositiveContactPlace(),
            'sample_date' => $iec->getSampleDate(),
            'sample_laboratory' => $iec->getSampleLaboratory(),
            'sample_type_id' => $iec->getSampleTypeId(),
            'symptom_date' => $iec->getSymptomDate(),
            'with_positive_contact' => $iec->withPositiveContact(),
        ];
    }
}
