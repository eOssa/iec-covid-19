<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SampleTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\SampleType $sampleType */
        $sampleType = $this;
        return [
            'id' => $sampleType->getKey(),
            'name' => $sampleType->getName(),
        ];
    }
}
