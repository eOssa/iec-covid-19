<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SymptomResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\Symptom $symptom */
        $symptom = $this;
        return [
            'id' => $symptom->getKey(),
            'name' => $symptom->getName(),
        ];
    }
}
