<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comorbidity extends Model
{
    use HasFactory;

    const PREGNANCY = 11;

    protected $guarded = [];
    public $timestamps = false;

    public function getName()
    {
        return $this->name;
    }

    public function isPregnancy()
    {
        return $this->getKey() === self::PREGNANCY;
    }
}
