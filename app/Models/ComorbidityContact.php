<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ComorbidityContact extends Pivot
{
    public function getPregnancyWeeks()
    {
        return (int) $this->additional;
    }
}
