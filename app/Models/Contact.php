<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Contact extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $casts = [
        'birthdate' => 'date',
    ];

    public function comorbidities()
    {
        return $this->belongsToMany(Comorbidity::class)->using(ComorbidityContact::class)->withPivot('additional');
    }

    public function displacements()
    {
        return $this->hasMany(Displacement::class);
    }

    public function hospitalizations()
    {
        return $this->hasMany(Hospitalization::class);
    }

    public function iecs()
    {
        return $this->hasMany(Iec::class);
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getAge()
    {
        $birthdate = $this->getBirthdate();
        if (!$birthdate->diffInYears()) {
            return $birthdate->diffInMonths();
        }
        return $birthdate->diffInYears();
    }

    public function getAgeMessure()
    {
        $dateStr = mb_strtolower(explode(' ', $this->getBirthdate()->longAbsoluteDiffForHumans())[1]);
        return Str::contains($dateStr, ['año', 'años']) ? 'year' : 'month';
    }

    /**
     * @return \Illuminate\Support\Carbon
     */
    public function getBirthdate()
    {
        return $this->birthdate ?? now();
    }

    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\App\Models\Comorbidity[]
     */
    public function getComorbidities()
    {
        return $this->comorbidities;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\App\Models\Displacement[]
     */
    public function getDisplacements()
    {
        return $this->displacements;
    }

    public function getEps()
    {
        return $this->eps;
    }

    public function getExplodedName()
    {
        $name = explode(' ', $this->getName(), 4);
        switch (count($name)) {
            case 0:
                return ['', '', '', ''];
            case 1:
                return [$name[0], '', '', ''];
            case 2:
                return [$name[0], $name[1], '', ''];
            case 3:
                return [$name[0], '', $name[1], $name[2]];
            default:
                return $name;
        }
    }

    public function getFirstLastName()
    {
        return $this->getExplodedName()[2];
    }

    public function getFirstName()
    {
        return $this->getExplodedName()[0];
    }

    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\App\Models\Hospitalization[]
     */
    public function getHospitalizations()
    {
        return $this->hospitalizations;
    }

    public function getIdentification()
    {
        return $this->identification;
    }

    /**
     * @return \App\Models\IdentificationType
     */
    public function getIdentificationType()
    {
        return $this->identificationType;
    }

    public function getIdentificationTypeId()
    {
        return $this->identification_type_id;
    }

    public function getIdentificationTypeName()
    {
        return $this->getIdentificationType()->getName();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getNationality()
    {
        return $this->nationality;
    }

    public function getNeighborhood()
    {
        return $this->neighborhood;
    }

    public function getOccupation()
    {
        return $this->occupation;
    }

    public function getPregnancyWeeks()
    {
        $comorbidity = $this->getComorbidities()->firstWhere('id', Comorbidity::PREGNANCY);
        if ($comorbidity && $comorbidity->pivot) {
            return $comorbidity->pivot->getPregnancyWeeks();
        }
        return 0;
    }

    public function getPrimaryPhone()
    {
        return $this->primary_phone;
    }

    /**
     * @return \App\Models\ContactIec|null
     */
    public function getPivot()
    {
        return $this->pivot;
    }

    public function getRegion()
    {
        return $this->region;
    }

    public function getSecondaryPhone()
    {
        return $this->secondary_phone;
    }

    public function getSecondLastName()
    {
        return $this->getExplodedName()[3];
    }

    public function getSecondName()
    {
        return $this->getExplodedName()[1];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\App\Models\Symptom[]
     */
    public function getSymptoms()
    {
        return $this->symptoms;
    }

    public function getZone()
    {
        return $this->zone;
    }

    public function identificationType()
    {
        return $this->belongsTo(IdentificationType::class)->withDefault();
    }

    public function isFemale()
    {
        return !$this->isMale();
    }

    public function isMale()
    {
        return $this->getGender() === 'M';
    }

    public function liveInRural()
    {
        return !$this->liveInUrban();
    }

    public function liveInUrban()
    {
        return $this->getZone() === 'Urbana';
    }

    public function symptoms()
    {
        return $this->belongsToMany(Symptom::class)->using(ContactSymptom::class)->withTimestamps();
    }
}
