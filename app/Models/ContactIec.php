<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ContactIec extends Pivot
{
    protected $casts = [
        'contact_date' => 'date',
        'sample_date' => 'date',
    ];

    /**
     * @return \Illuminate\Support\Carbon
     */
    public function getContactDate()
    {
        return $this->contact_date;
    }

    public function getRelationship()
    {
        return $this->relationship;
    }

    public function getRelationshipType()
    {
        switch (mb_strtolower($this->getRelationship())) {
            case 'abuela':
            case 'abuelo':
            case 'bisabuela':
            case 'bisabuelo':
            case 'bisnieta':
            case 'bisnieto':
            case 'cuñada':
            case 'cuñado':
            case 'esposa':
            case 'esposo':
            case 'hermana':
            case 'hermano':
            case 'hija':
            case 'hijo':
            case 'madrastra':
            case 'madre':
            case 'nieta':
            case 'nieto':
            case 'padrastro':
            case 'padre':
            case 'prima':
            case 'primo':
            case 'suegra':
            case 'suegro':
            case 'tía':
            case 'tío':
            case 'tia':
            case 'tio':
            case 'yerno':
            case 'yerna':
                return 'Familiar';
            default:
                return 'Social';
        }
    }

    /**
     * @return \Illuminate\Support\Carbon
     */
    public function getSampleDate()
    {
        return $this->sample_date;
    }

    public function getSampleDateString($format = 'Y-m-d')
    {
        return $this->getSampleDate() ? $this->getSampleDate()->format($format) : '';
    }

    public function getSampleResult()
    {
        return $this->sample_result;
    }
}
