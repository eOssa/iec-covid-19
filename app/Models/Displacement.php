<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Displacement extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $casts = [
        'end_date' => 'date',
        'start_date' => 'date',
    ];

    public function getCity()
    {
        return $this->city;
    }

    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return \Illuminate\Support\Carbon
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * @return \Illuminate\Support\Carbon
     */
    public function getStartDate()
    {
        return $this->start_date;
    }
}
