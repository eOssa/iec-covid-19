<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    use HasFactory;

    const ANTIGEN = 1;
    const PCR = 2;

    protected $guarded = [];
    public $timestamps = false;

    public function getName()
    {
        return $this->name;
    }
}
