<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hospitalization extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $casts = [
        'date' => 'date',
    ];

    /**
     * @return \Illuminate\Support\Carbon
     */
    public function getDate()
    {
        return $this->date;
    }

    public function getInstitution()
    {
        return $this->institution;
    }
}
