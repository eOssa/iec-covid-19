<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\{Model, SoftDeletes};

class Iec extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $casts = [
        self::CREATED_AT => 'datetime',
        'date' => 'date',
        'sample_date' => 'date',
        'symptom_start_date' => 'date',
        'with_positive_contact' => 'boolean',
    ];

    public function contact()
    {
        return $this->belongsTo(Contact::class)->withDefault();
    }

    public function contacts()
    {
        return $this->belongsToMany(Contact::class)
            ->using(ContactIec::class)
            ->withPivot(['contact_date', 'relationship', 'sample_date', 'sample_result'])
            ->withTimestamps();
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return \Illuminate\Support\Carbon
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return \App\Models\Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\App\Models\Contact[]
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    public function getExamId()
    {
        return $this->exam_id;
    }

    public function getMedication()
    {
        return $this->medication;
    }

    public function getMedications()
    {
        if (!$this->getMedication()) {
            return [];
        }
        return explode(',', $this->getMedication());
    }

    public function getNursingNote()
    {
        return $this->nursing_note;
    }

    public function getPositiveContactPlace()
    {
        return $this->positive_contact_place;
    }

    /**
     * @return \Illuminate\Support\Carbon
     */
    public function getSampleDate()
    {
        return $this->sample_date;
    }

    public function getSampleLaboratory()
    {
        return $this->sample_laboratory;
    }

    public function getSampleTypeId()
    {
        return $this->sample_type_id;
    }

    /**
     * @return \Illuminate\Support\Carbon
     */
    public function getSymptomDate()
    {
        return $this->symptom_start_date;
    }

    public function withPositiveContact()
    {
        return $this->with_positive_contact;
    }
}
