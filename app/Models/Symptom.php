<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Symptom extends Model
{
    use HasFactory;

    const FEVER = 1;
    const COUGH = 2;
    const BREATHING_DIFFICULTY = 3;
    const TACHYPNEA = 4;
    const SORE_THROAT = 5;
    const SHIVERS = 6;
    const NAUSEAS = 7;
    const VOMIT = 8;
    const CHEST_PAIN = 9;
    const MYALGIA = 10;
    const DIARRHEA = 11;
    const ABDOMINAL_PAIN = 12;
    const HEADACHE = 13;
    const GENERAL_UNREST = 14;
    const LOSS_OF_SMELL_AND_TASTE = 15;

    protected $guarded = [];
    public $timestamps = false;

    public function getName()
    {
        return $this->name;
    }
}
