<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    private $repositories = [
        \App\Contracts\Repositories\ComorbiditiesRepositoryInterface::class => \App\Repositories\ComorbiditiesRepository::class,
        \App\Contracts\Repositories\ExamsRepositoryInterface::class => \App\Repositories\ExamsRepository::class,
        \App\Contracts\Repositories\IdentificationTypesRepositoryInterface::class => \App\Repositories\IdentificationTypesRepository::class,
        \App\Contracts\Repositories\IecsRepositoryInterface::class => \App\Repositories\IecsRepository::class,
        \App\Contracts\Repositories\SamplesTypesRepositoryInterface::class => \App\Repositories\SamplesTypesRepository::class,
        \App\Contracts\Repositories\SymptomsRepositoryInterface::class => \App\Repositories\SymptomsRepository::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->repositories as $interface => $repository) {
            $this->app->bind($interface, $repository);
        }
    }
}
