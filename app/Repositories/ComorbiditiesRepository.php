<?php

namespace App\Repositories;

use App\Contracts\Repositories\ComorbiditiesRepositoryInterface;
use App\Models\Comorbidity as Model;

class ComorbiditiesRepository extends Repository implements ComorbiditiesRepositoryInterface
{
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}
