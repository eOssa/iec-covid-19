<?php

namespace App\Repositories;

use App\Contracts\Repositories\ExamsRepositoryInterface;
use App\Models\Exam as Model;

class ExamsRepository extends Repository implements ExamsRepositoryInterface
{
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}
