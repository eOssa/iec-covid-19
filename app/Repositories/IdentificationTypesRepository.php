<?php

namespace App\Repositories;

use App\Contracts\Repositories\IdentificationTypesRepositoryInterface;
use App\Models\IdentificationType as Model;

class IdentificationTypesRepository extends Repository implements IdentificationTypesRepositoryInterface
{
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}
