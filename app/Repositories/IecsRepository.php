<?php

namespace App\Repositories;

use App\Contracts\Repositories\IecsRepositoryInterface;
use App\Models\Iec as Model;

class IecsRepository extends Repository implements IecsRepositoryInterface
{
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}
