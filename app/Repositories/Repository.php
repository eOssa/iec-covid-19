<?php

namespace App\Repositories;

use App\Contracts\Repositories\RepositoryInterface;

class Repository implements RepositoryInterface
{
    /**
     * The model instance.
     *
     * @var \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    protected $model;

    /**
     * @inheritDoc
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * @inheritDoc
     */
    public function paginate($page, $perPage = 10)
    {
        return $this->model->paginate($perPage, ['*'], 'page', $page);
    }
}
