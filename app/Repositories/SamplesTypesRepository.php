<?php

namespace App\Repositories;

use App\Contracts\Repositories\SamplesTypesRepositoryInterface;
use App\Models\SampleType as Model;

class SamplesTypesRepository extends Repository implements SamplesTypesRepositoryInterface
{
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}
