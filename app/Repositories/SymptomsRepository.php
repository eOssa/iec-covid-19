<?php

namespace App\Repositories;

use App\Contracts\Repositories\SymptomsRepositoryInterface;
use App\Models\Symptom as Model;

class SymptomsRepository extends Repository implements SymptomsRepositoryInterface
{
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}
