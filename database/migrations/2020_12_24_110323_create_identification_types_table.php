<?php

use App\Models\IdentificationType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIdentificationTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('identification_types', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('name', 2);
        });
        IdentificationType::create(['name' => 'CC']);
        IdentificationType::create(['name' => 'TI']);
        IdentificationType::create(['name' => 'RC']);
        IdentificationType::create(['name' => 'CE']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('identification_types');
    }
}
