<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->string('eps', 20);
            $table->string('name', 45);
            $table->unsignedTinyInteger('identification_type_id');
            $table->string('identification', 15);
            $table->date('birthdate')->nullable();
            $table->string('gender', 1);
            $table->string('nationality', 15)->nullable();
            $table->string('address');
            $table->string('region', 20)->nullable();
            $table->string('city', 20)->nullable();
            $table->string('neighborhood')->nullable();
            $table->string('zone', 6)->nullable();
            $table->string('primary_phone', 15);
            $table->string('secondary_phone', 15)->nullable();
            $table->string('occupation')->nullable();
            $table->timestamps();

            $table->foreign('identification_type_id')
                ->references('id')
                ->on('identification_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
