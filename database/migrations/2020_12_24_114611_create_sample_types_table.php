<?php

use App\Models\SampleType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSampleTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sample_types', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('name', 30);
        });
        SampleType::create(['name' => 'Aspirado traqueal']);
        SampleType::create(['name' => 'Hisopado nasofaríngeo']);
        SampleType::create(['name' => 'Lavado bronco alveolar']);
        SampleType::create(['name' => 'Aspirado nasofaríngeo']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sample_types');
    }
}
