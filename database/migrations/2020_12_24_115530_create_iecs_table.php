<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iecs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('contact_id');
            $table->date('date');
            $table->date('symptom_start_date');
            $table->boolean('with_positive_contact')->default(false);
            $table->string('positive_contact_place')->nullable();
            $table->string('medication');
            $table->date('sample_date');
            $table->string('sample_laboratory', 45)->nullable();
            $table->unsignedTinyInteger('sample_type_id');
            $table->unsignedTinyInteger('exam_id');
            $table->text('nursing_note')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('contact_id')
                ->references('id')
                ->on('contacts');
            $table->foreign('sample_type_id')
                ->references('id')
                ->on('sample_types');
            $table->foreign('exam_id')
                ->references('id')
                ->on('exams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('iecs');
    }
}
