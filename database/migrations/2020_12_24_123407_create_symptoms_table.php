<?php

use App\Models\Symptom;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSymptomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('symptoms', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('name');
        });
        Symptom::create(['name' => 'Fiebre cuantificada']);
        Symptom::create(['name' => 'Tos']);
        Symptom::create(['name' => 'Dificultad respiratoria']);
        Symptom::create(['name' => 'Taquipnea']);
        Symptom::create(['name' => 'Dolor de garganta']);
        Symptom::create(['name' => 'Escalofríos']);
        Symptom::create(['name' => 'Nauseas']);
        Symptom::create(['name' => 'Vomito']);
        Symptom::create(['name' => 'Dolor torácico']);
        Symptom::create(['name' => 'Mialgia']);
        Symptom::create(['name' => 'Diarrea']);
        Symptom::create(['name' => 'Dolor abdominal']);
        Symptom::create(['name' => 'Dolor de cabeza']);
        Symptom::create(['name' => 'Malestar general']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('symptoms');
    }
}
