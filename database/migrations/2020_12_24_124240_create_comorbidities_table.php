<?php

use App\Models\Comorbidity;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComorbiditiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comorbidities', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('name');
        });
        Comorbidity::create(['name' => 'Asma']);
        Comorbidity::create(['name' => 'Enfermedad pulmonar crónica']);
        Comorbidity::create(['name' => 'Trastorno neurológico crónico']);
        Comorbidity::create(['name' => 'Inmunosupresión']);
        Comorbidity::create(['name' => 'Enfermedad renal crónica']);
        Comorbidity::create(['name' => 'Enfermedad cardiaca']);
        Comorbidity::create(['name' => 'Enfermedad hematológica crónica']);
        Comorbidity::create(['name' => 'Diabetes']);
        Comorbidity::create(['name' => 'Obesidad']);
        Comorbidity::create(['name' => 'Enfermedad hepática crónica']);
        Comorbidity::create(['name' => 'Embarazo']);
        Comorbidity::create(['name' => 'Tabaquismo']);
        Comorbidity::create(['name' => 'Alcoholismo']);
        Comorbidity::create(['name' => 'Trastorno reumatológico']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comorbidities');
    }
}
