<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactIecTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_iec', function (Blueprint $table) {
            $table->unsignedBigInteger('contact_id');
            $table->unsignedBigInteger('iec_id');
            $table->string('relationship', 20);
            $table->date('contact_date');
            $table->timestamps();

            $table->primary(['contact_id', 'iec_id']);
            $table->foreign('contact_id')
                ->references('id')
                ->on('contacts');
            $table->foreign('iec_id')
                ->references('id')
                ->on('iecs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_iec');
    }
}
