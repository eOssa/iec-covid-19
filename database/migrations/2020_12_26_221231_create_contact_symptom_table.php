<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactSymptomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_symptom', function (Blueprint $table) {
            $table->unsignedBigInteger('contact_id');
            $table->unsignedTinyInteger('symptom_id');
            $table->timestamps();

            $table->primary(['contact_id', 'symptom_id']);
            $table->foreign('contact_id')
                ->references('id')
                ->on('contacts');
            $table->foreign('symptom_id')
                ->references('id')
                ->on('symptoms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_symptom');
    }
}
