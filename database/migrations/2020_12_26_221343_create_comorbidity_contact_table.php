<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComorbidityContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comorbidity_contact', function (Blueprint $table) {
            $table->unsignedTinyInteger('comorbidity_id');
            $table->unsignedBigInteger('contact_id');
            $table->string('additional')->nullable()->comment('Used in pregnancy weeks');
            $table->timestamps();

            $table->primary(['comorbidity_id', 'contact_id']);
            $table->foreign('comorbidity_id')
                ->references('id')
                ->on('comorbidities');
            $table->foreign('contact_id')
                ->references('id')
                ->on('contacts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comorbidity_contact');
    }
}
