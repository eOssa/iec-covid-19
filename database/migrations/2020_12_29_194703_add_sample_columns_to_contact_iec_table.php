<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSampleColumnsToContactIecTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contact_iec', function (Blueprint $table) {
            $table->date('sample_date')->nullable();
            $table->string('sample_result', 20)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contact_iec', function (Blueprint $table) {
            $table->dropColumn(['sample_date', 'sample_result']);
        });
    }
}
