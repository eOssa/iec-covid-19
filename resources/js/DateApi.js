const DateApi = {
  diffInDays(from, to = null) {
    return Math.ceil(
      this.diff(from, to) / 1000 / 60 / 60 / 24
    );
  },
  diff(from, to = null) {
    if (to === null) {
      to = new Date();
    }
    return from - to;
  },
  diffInYears(from, to = null) {
    if (to === null) {
      to = new Date();
    }
    let ynew = to.getFullYear();
    let mnew = to.getMonth();
    let dnew = to.getDate();
    let yold = from.getFullYear();
    let mold = from.getMonth();
    let dold = from.getDate();
    let diff = ynew - yold;
    if (mold > mnew) diff--;
    else {
        if (mold == mnew) {
            if (dold > dnew) diff--;
        }
    }
    return diff;
  },
};

export default DateApi;
