const Str = {
  slug(value, separator = '_') {
    return value.replace(/^\s\s*/, '')
      .replace(/\s\s*$/, '')
      .toLowerCase()
      .replace(/[^a-z0-9_\-~!+\s]+/g, '')
      .replace(/[\s]+/g, separator);
  },
  ucfirst(value) {
    return value.charAt(0).toUpperCase() + value.slice(1);
  }
}

export default Str;
