/* eslint-disable vue/one-component-per-file */
import { createApp } from 'vue';
import route from 'ziggy';
import { Ziggy } from './ziggy';
import AppContainer from './components/AppContainer.vue';
import AppHeader from './components/AppHeader.vue';
import AppHeaderButton from './components/AppHeaderButton.vue';
import AppNavbar from './components/AppNavbar.vue';
import FormIec from './components/FormIec.vue';
import TableIecs from './components/TableIecs.vue';

require('./bootstrap');

const app = createApp({});

app.mixin({
  methods: {
    isArray(value) {
      return Object.prototype.toString.call(value) === '[object Array]';
    },
    isNumber(value) {
      return typeof value === 'number';
    },
    route: (name, params, absolute, config = Ziggy) => route(name, params, absolute, config),
  },
});

app.component('AppContainer', AppContainer);
app.component('AppHeader', AppHeader);
app.component('AppHeaderButton', AppHeaderButton);
app.component('AppNavbar', AppNavbar);
app.component('FormIec', FormIec);
app.component('TableIecs', TableIecs);

app.mount('#app');
