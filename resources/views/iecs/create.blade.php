@extends('layouts.app')

@php
  $isEdit = isset($iec['id']);
@endphp

@section('header')
  Agregar IEC
  <template #actions>
    @if ($isEdit)
      <app-header-button href="{{ route('iecs.contacts.report', $iec['id']) }}">
        Generar Excel
      </app-header-button>
      <app-header-button href="{{ route('iecs.report', $iec['id']) }}">
        Generar Word
      </app-header-button>
    @endif
    <app-header-button href="{{ route('iecs.create') }}">
      Agregar IEC
    </app-header-button>
  </template>
@endsection

@section('content')
  @php
    $iec = $iec ?? [];
    $contact = Arr::get($iec, 'contact', []);
  @endphp
  <app-container>
    <form-iec
      :id="{{ Arr::get($iec, 'id', 0) }}"
      initial-address="{{ Arr::get($contact, 'address') }}"
      initial-birthdate="{{ Arr::get($contact, 'birthdate') }}"
      initial-city="{{ Arr::get($contact, 'city', 'Pereira') }}"
      initial-date="{{ Arr::get($iec, 'date', now()->toDateString()) }}"
      :initial-comorbidities="@json(Arr::get($contact, 'comorbidities', []))"
      :initial-contacts='@json(Arr::get($iec, 'contacts', []))'
      :initial-displacements='@json(Arr::get($contact, 'displacements', []))'
      initial-eps="{{ Arr::get($contact, 'eps') }}"
      :initial-exam-id="{{ Arr::get($iec, 'exam_id', 1) }}"
      initial-gender="{{ Arr::get($contact, 'gender') }}"
      :initial-hospitalizations='@json(Arr::get($contact, 'hospitalizations', []))'
      initial-identification="{{ Arr::get($contact, 'identification') }}"
      :initial-identification-type-id="{{ Arr::get($contact, 'identification_type_id', 1) }}"
      :initial-medication='@json(Arr::get($iec, 'medication', []))'
      initial-name="{{ Arr::get($contact, 'name') }}"
      initial-nationality="{{ Arr::get($contact, 'nationality', 'Colombiano') }}"
      initial-neighborhood="{{ Arr::get($contact, 'neighborhood') }}"
      initial-nursing-note="{{ Arr::get($iec, 'nursing_note') }}"
      initial-occupation="{{ Arr::get($contact, 'occupation') }}"
      initial-positive-contact-place="{{ Arr::get($iec, 'positive_contact_place') }}"
      :initial-pregnancy-weeks="{{ Arr::get($contact, 'pregnancy_weeks', 0) }}"
      initial-primary-phone="{{ Arr::get($contact, 'primary_phone') }}"
      initial-region="{{ Arr::get($contact, 'region', 'Risaralda') }}"
      initial-sample-date="{{ Arr::get($iec, 'sample_date') }}"
      initial-sample-laboratory="{{ Arr::get($iec, 'sample_laboratory') }}"
      :initial-sample-type-id="{{ Arr::get($iec, 'sample_type_id', 2) }}"
      initial-secondary-phone="{{ Arr::get($contact, 'secondary_phone') }}"
      initial-symptom-date="{{ Arr::get($iec, 'symptom_date') }}"
      :initial-symptoms='@json(Arr::get($contact, 'symptoms', []))'
      :initial-with-positive-contact="@json(Arr::get($iec, 'with_positive_contact', false))"
      initial-zone="{{ Arr::get($contact, 'zone', 'Urbana') }}"
    >
      <template #token>
        @csrf
      </template>
    </form-iec>
  </app-container>
@endsection
