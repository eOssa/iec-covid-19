@extends('layouts.app')

@section('header')
  Listado de IECs
  <template #actions>
    <app-header-button :href="route('iecs.create')">
      Agregar IEC
    </app-header-button>
  </template>
@endsection

@section('content')
  <app-container>
    <table-iecs></table-iecs>
  </app-container>
@endsection
