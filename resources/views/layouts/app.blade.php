<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>{{ config('app.name') }}</title>
  <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>

<div id="app">
  <app-navbar></app-navbar>
  <app-header>
    @yield('header')
  </app-header>
  <main>
    @yield('content')
  </main>
</div>
<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
