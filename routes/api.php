<?php

use App\Http\Controllers\{IecContactsReportController, IecFoldersReportController, IecReportController};
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('iecs/report', IecReportController::class); // @deprecated
Route::post('iecs/report', IecReportController::class);
Route::post('iecs/folders-report', IecFoldersReportController::class);
Route::get('iecs/contacts/report', IecContactsReportController::class); // @deprecated
Route::post('iecs/contacts/report', IecContactsReportController::class);
// Route::name('api.')->group(function () {
//     Route::apiResources([
//         'comorbidities' => \App\Http\Controllers\ComorbidityController::class,
//         'exams' => \App\Http\Controllers\ExamController::class,
//         'iecs' => \App\Http\Controllers\IecController::class,
//         'identifications-types' => \App\Http\Controllers\IdentificationTypeController::class,
//         'samples-types' => \App\Http\Controllers\SampleTypeController::class,
//         'symptoms' => \App\Http\Controllers\SymptomController::class,
//     ]);
// });
