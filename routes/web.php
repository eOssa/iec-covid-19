<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::view('/', 'welcome');
// Route::get('iecs/{iec}/report', \App\Http\Controllers\IecReportController::class)->name('iecs.report');
// Route::get('iecs/{iec}/contacts/report', \App\Http\Controllers\IecContactsReportController::class)->name('iecs.contacts.report');
// Route::resources([
//     'comorbidities' => \App\Http\Controllers\ComorbidityController::class,
//     'exams' => \App\Http\Controllers\ExamController::class,
//     'iecs' => \App\Http\Controllers\IecController::class,
//     'identifications-types' => \App\Http\Controllers\IdentificationTypeController::class,
//     'samples-types' => \App\Http\Controllers\SampleTypeController::class,
//     'symptoms' => \App\Http\Controllers\SymptomController::class,
// ]);
