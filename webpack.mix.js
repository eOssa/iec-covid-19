const mix = require('laravel-mix');
const path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
  .vue()
  .css('resources/css/app.css', 'public/css', [
    require("tailwindcss")
  ])
  .alias({
    vue$: path.join(__dirname, 'node_modules/vue/dist/vue.esm-bundler.js'),
    ziggy: path.resolve('vendor/tightenco/ziggy/dist'),
  })
  .webpackConfig(webpack => {
    return {
      plugins: [
        new webpack.DefinePlugin({__VUE_PROD_DEVTOOLS__: mix.inProduction(), __VUE_OPTIONS_API__: true}),
      ],
    };
  })
  .version();
